from django import forms

class BookSearch(forms.Form):
    keyword = forms.CharField(label='Cari Buku', max_length=100)