$(document).ready(function(){
    
    $("#keyword").change(function(){
        booksearch();
    })
    
    $("#search").click(function(){
        booksearch();
    })
})

function booksearch() {
    var keyword = $("#keyword").val();
    $.ajax({
        url: "search/?q=" + keyword,
        
        success: function(result){
            $("#search_result").empty();
            var items = result.items;
            
            $("#search_result").append(
                '<table class="table">' +
                '<thead>' + '<tr>' +
                '<th scope="col">Judul Buku</th>' +
                '<th scope="col">Penerbit</th>' + 
                '<th scope="col">Gambar</th>' +
                '</tr>' + '</thead>' +
                '<tbody>'
            )
            
            
            for (i = 0; i < result.items.length; i++) {
                
                var judul = items[i].volumeInfo.title;
                var penerbit = items[i].volumeInfo.publisher;
                var gambar = items[i].volumeInfo.imageLinks.smallThumbnail;
                
                $("#search_result").append(
                    '<tr>' + 
                    '<th scope="row">' + judul + '</th>' +  
                    '<td>' + penerbit + '</td>' +
                    '<td><img src=' + gambar + '></td>' + '</tr>'
                )
            }
            
            $("#search_result").append(
                '</tbody>' + '</table>'
            )
        }
        
    
        });
}