from django.test import TestCase, Client
from django.urls import resolve
from .views import index, booksearch

class KegiatanTest(TestCase):
    
    def test_booksearch_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
    
    def test_home_using_booksearch_function(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)
    
    def test_home_using_booksearch_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'booksearch.html')