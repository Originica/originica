from django.urls import path, re_path
from booksearch import views

urlpatterns = [
    path('', views.index, name='index'),
    path('search/', views.booksearch, name='booksearch')
]