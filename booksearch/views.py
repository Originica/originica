from django.shortcuts import render
from django.http import JsonResponse
import requests, json

def index(request):
    return render(request, 'booksearch.html')

def booksearch(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    data = json.loads(requests.get(url).content)
    return JsonResponse(data, safe=False)
