from django.shortcuts import render

def home(request):
	return render(request, 'home.html')
	
def about(request):
	return render(request, 'about.html')

def experience(request):
	return render(request, 'experience.html')

def achievements(request):
	return render(request, 'achievements.html')

def contact(request):
	return render(request, 'contact.html')