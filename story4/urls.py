from django.urls import include, path
from story4 import views

urlpatterns = [
	path('', views.home, name='home'),
	path('about', views.about, name='about'),
	path('experience', views.experience, name='experience'),
	path('achievements', views.achievements, name='achievements'),
	path('contact', views.contact, name='contact'),
]