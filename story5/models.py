from django.db import models

class MataKuliah(models.Model):
    nama = models.CharField(max_length=40)
    dosen = models.CharField(max_length=40)
    sks = models.IntegerField()
    deskripsi = models.TextField()
    semester = models.CharField(max_length=15)
    ruang = models.CharField(max_length=15)
