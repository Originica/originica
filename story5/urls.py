from django.urls import include, path
from story5 import views

urlpatterns = [
	path('', views.home, name='home'),
	path('about', views.about, name='about'),
	path('experience', views.experience, name='experience'),
	path('achievements', views.achievements, name='achievements'),
	path('projects', views.projects, name='projects'),
    path('form', views.form, name='form'),
    path('matkul', views.matkul, name='matkul'),
    path('detailmatkul/<str:nama>', views.detailmatkul, name='detailmatkul')
]