from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import FormMatkul
from .models import MataKuliah

def home(request):
	return render(request, 'story5/home.html')
	
def about(request):
	return render(request, 'story5/about.html')

def experience(request):
	return render(request, 'story5/experience.html')

def achievements(request):
	return render(request, 'story5/achievements.html')

def projects(request):
	return render(request, 'story5/projects.html')

def form(request):
    context = {}
    if (request.method == 'POST'):
        form = FormMatkul(request.POST or None)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('form')
    else:
        form = FormMatkul()
        context['form'] = form
        return render(request, 'story5/form.html', context)

def matkul(request):
    matkul = MataKuliah.objects.all()
    context = { 'matkul':matkul }
    return render(request, 'story5/matkul.html', context)

def detailmatkul(request, nama):
    matkul = MataKuliah.objects.get(nama=nama)
    context = { 'matkul':matkul }
    if (request.method == 'POST'):
        matkul.delete()
        return HttpResponseRedirect('/story5/matkul')
    return render(request, 'story5/detailmatkul.html', context)
