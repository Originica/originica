from django import forms
from .models import Kegiatan, Peserta

class FormPeserta(forms.ModelForm):
    
    class Meta:
        model = Peserta
        fields = ('nama',)