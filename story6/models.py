from django.db import models

class Kegiatan(models.Model):
    kegiatan = models.CharField(max_length=40)
    
    def __str__(self):
        return self.kegiatan
    
class Peserta(models.Model):
    nama = models.CharField(max_length=40)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nama