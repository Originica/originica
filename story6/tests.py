from django.test import TestCase, Client
from django.urls import resolve
from .views import kegiatan, form
from .models import Kegiatan, Peserta
from .forms import FormPeserta

class KegiatanTest(TestCase):
    
    def test_home_url_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)
    
    def test_home_using_kegiatan_function(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, kegiatan)
    
    def test_home_using_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'kegiatan.html')

class FormTest(TestCase):
    
    def test_form_url_is_exist(self):
        obj = Kegiatan(kegiatan='test')
        obj.save()
        id = obj.id
        response = Client().get('/story6/form/'+str(id))
        self.assertEqual(response.status_code, 200)
        
    def test_form_using_form_function(self):
        obj = Kegiatan(kegiatan='test')
        obj.save()
        id = obj.id
        found = resolve('/story6/form/'+str(id))
        self.assertEqual(found.func, form)
    
    def test_form_using_template(self):
        obj = Kegiatan(kegiatan='test')
        obj.save()
        id = obj.id
        response = Client().get('/story6/form/'+str(id))
        self.assertTemplateUsed(response, 'tambahpeserta.html')
        
    def test_form_creates_new_object(self):
        obj = Kegiatan(kegiatan='test')
        obj.save()
        id = obj.id
        form_data = {'nama':'test', 'kegiatan':Kegiatan(kegiatan='test')}
        response = Client().post('/story6/form/'+str(id), data=form_data)
        orang = Peserta.objects.get(nama='test')
        self.assertEqual(str(orang), orang.nama)