from django.urls import path
from story6 import views

urlpatterns = [
    path('', views.kegiatan, name='home'),
    path('form/<int:id>', views.form, name='form'),
]