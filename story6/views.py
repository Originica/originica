from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import FormPeserta
from .models import Kegiatan, Peserta

def kegiatan(request):
    context = {}
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    context['kegiatan'] = kegiatan
    context['peserta'] = peserta
    return render(request, 'kegiatan.html', context)

def form(request, id):
    context = {}
    kegiatan = Kegiatan.objects.get(id=id)
    if (request.method == 'POST'):
        form = FormPeserta(request.POST or None)
        if form.is_valid():
            peserta = form.save(commit=False)
            peserta.kegiatan = kegiatan
            peserta.save()
            return HttpResponseRedirect('/story6')
    else:
        form = FormPeserta()
        context['form'] = form
        context['kegiatan'] = Kegiatan.objects.get(id=id)
        return render(request, 'tambahpeserta.html', context)